import { AwesomeLocation } from "./awesome-location";

export class Place {
    constructor(public title: string, 
        public description: string,
        public location: AwesomeLocation,
        public imageUrl: string) {

    }
}