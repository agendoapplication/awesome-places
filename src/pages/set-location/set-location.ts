import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { AwesomeLocation } from "../../models/awesome-location";

@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html'
})
export class SetLocationPage {

  location: AwesomeLocation;
  marker: AwesomeLocation;

  constructor(public navCtrl: NavController,
    private navParams: NavParams,
    private viewCtrl: ViewController) {
    const paramLocation = navParams.get('location');
    if (paramLocation) {
      this.location = paramLocation;
    }
    const paramIsSet = navParams.get('isSet');
    if (paramIsSet) {
      this.marker = this.location;
    }
  }

  onSetMarker(event: any) {
    this.marker = new AwesomeLocation(event.coords.lat, event.coords.lng);
  }

  onConfirm() {
    this.viewCtrl.dismiss({location: this.marker});
  }

  onAbort() {
    this.viewCtrl.dismiss();
  }
}