import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { SetLocationPage } from "../set-location/set-location";
import { AwesomeLocation } from "../../models/awesome-location";
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { File, Entry, FileError } from '@ionic-native/file';
import { PlacesService } from "../../services/places";

declare var cordova: any;

@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html'
})
export class AddPlacePage {

  location: AwesomeLocation = {
    lat:  40.7624324,
    lng: -73.9759827
  };
  locationIsSet = false;
  imageUrl = '';

  constructor(public navCtrl: NavController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private geolocation: Geolocation,
    private camera: Camera,
    private file: File,
    private placesService: PlacesService) {

  }

  onLocate() {
    const loader = this.loadingCtrl.create({
      content: 'Getting your location...'
    });
    loader.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      loader.dismiss();
      this.location.lat = resp.coords.latitude;
      this.location.lng = resp.coords.longitude;
      this.locationIsSet = true;
    }).catch((error) => {
      loader.dismiss();
      const toast = this.toastCtrl.create({
        message: 'Could not get location, please pick it manually',
        duration: 2500
      });
      toast.present();
    });
  }

  onOpenMap() {
    const modal = this.modalCtrl.create(
      SetLocationPage, 
      {
        location: this.location,
        isSet: this.locationIsSet
      }
    );
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        this.location = data.location;
        this.locationIsSet = true;
      }
    });
  }

  onTakePhoto() {
    this.camera.getPicture({
      encodingType : this.camera.EncodingType.JPEG,
      correctOrientation: true
    })
    .then(
      (imageData) => {
        const currentName = imageData.replace(/^.*[\\\/]/, '');
        const path = imageData.replace(/[^\/]*$/, '');
        const newFileName = new Date().getUTCMilliseconds() + ".jpg";
        this.file.moveFile(path, currentName, cordova.file.dataDirectory, newFileName)
          .then(
            data => {
              this.imageUrl = data.nativeURL;
              this.camera.cleanup();
              //this.file.removeFile(path, currentName);
            }
          )
          .catch(
            (error: FileError) => {
              this.imageUrl = '';
              const toast = this.toastCtrl.create({
                message: 'Could not save the image: ' + error.message,
                duration: 2500
              });
              toast.present();
              this.camera.cleanup();
            }
          );
        this.imageUrl = imageData;
      }
    )
    .catch(
      error => {
        const toast = this.toastCtrl.create({
          message: 'Could not take a picture' + error.message,
          duration: 2500
        });
        toast.present();
      }
    )
  }

  onSubmit(form: NgForm) {
    this.placesService.addPlace(
      form.value.title, 
      form.value.description,
      this.location,
      this.imageUrl);
    form.reset();
    this.location = {
      lat:  40.7624324,
      lng: -73.9759827
    };
    this.locationIsSet = false;
    this.imageUrl = '';
    this.navCtrl.pop();
  }
}